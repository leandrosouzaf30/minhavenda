<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url('dashboard/index');?>">Minha Venda</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url('cliente/visualizar_todos');?>">Clientes</a></li>
            <li><a href="<?php echo base_url('produto/visualizar_todos');?>">Produtos</a></li>
            <li><a href="<?php echo base_url('pedido/visualizar_todos');?>">Pedidos</a></li>
            <li><a href="<?php echo base_url('venda/visualizar');?>">Vendas</a></li>
          </ul>
          
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          
          <ul class="nav nav-sidebar">
             <li class="active"><a href="<?php echo base_url('dashboard/index');?>">Início <span class="sr-only">(current)</span></a></li>
             <li class="button-dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle">
                Clientes<span>▼</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="<?php echo base_url('cliente/visualizar_todos');?>">
                    Visualizar Clientes
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('cliente/cadastrar_cliente'); ?>">
                    Cadastrar novo Cliente
                  </a>
                </li>
                
              </ul>
            </li>

            <li class="button-dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle">
                Produtos/Estoque<span>▼</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="<?php echo base_url('produto/visualizar_todos');?>">
                    Visualizar Produtos
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('produto/cadastrar_produto'); ?>">
                    Cadastrar novo Produto
                  </a>
                </li>
              </ul>
            </li>

            <li>
              <a href="<?php echo base_url('pedido/visualizar_todos');?>">
                Pedidos<span></span>
              </a>
             </li>

             <li>
              <a href="<?php echo base_url('venda/visualizar');?>">
                Vendas<span></span>
              </a>
             </li>
           </ul>
          </div>