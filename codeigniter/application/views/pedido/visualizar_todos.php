  	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  		
        <h1 class="page-header">Pedidos</h1>
	  	<div class="container">	
		  	<div class="row">
		  		<div class="col-md-11">
		  			<table class="table table-striped">
	  					<thead>
								<tr>
									<th>ID</th>
									<th>Cliente</th>
									<th>Produto</th>
									<th>Quantidade</th>
									<th>Valor</th>
									<th>Ações</th>
									
								</tr>
							</thead>
							<tbody>
								<?php 
									if($pedidos){
										foreach ($pedidos as $pedido) 
								{ ?>
									<tr>
										<td><?php echo $pedido["id_pedido_item"]; ?></td>
										<td><?php echo $pedido["nome_cliente"]; ?></td>
										<td><?php echo $pedido["nome_produto"]; ?></td>
										<td><?php echo $pedido["qtd_pedido_item"]; ?></td>
										<td>

											<?php 
												 $valor_final = $pedido["preco_produto"] * $pedido['qtd_pedido_item']
											?>

											<?php echo $valor_final?>
												
										</td>
										
										<!--<td>
										<a class="btn btn-default" href="<?php echo base_url('produto/editar_produto/'.$produto["id_produto"]); ?>">
											<i class="glyphicon glyphicon-pencil"></i>
										</a>
										<a class="btn btn-danger" href="<?php echo base_url('produto/deletar_produto/'.$produto["id_produto"]);?>"  onclick="return confirm('Deseja deletar este produto?')">
											<i class="glyphicon glyphicon-trash"></i>
										</a>
									</td>-->
									</tr>
								<?php 
									} //final do foreach
								  } else{
								?>
								<tr>
									<td colspan="3" class="text-center">Não há usuários cadastrados</td>
								</tr>
								<?php
									}
								?>
							</tbody>
					</table>
		  		</div>
		  	</div>
	  	</div>

	    