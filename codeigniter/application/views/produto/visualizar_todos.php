  	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  		<h4 class="page-header">Cadastrar novo Produto 
          <a class="btn btn-default" href="<?php echo base_url('produto/cadastrar_produto'); ?>">
			<i class="glyphicon glyphicon-plus"></i>
		  </a>
		 </h4>

        <h1 class="page-header">Produtos em estoque</h1>
	  	<div class="container">	
		  	<div class="row">
		  		<div class="col-md-11">
		  			<table class="table table-striped">
	  					<thead>
								<tr>
									<th>ID</th>
									<th>Nome</th>
									<th>Preço</th>
									<th>Descrição</th>
									<th>Quantidade</th>
									<th>Categoria</th>
									<th>Ações</th>
									
								</tr>
							</thead>
							<tbody>
								<?php 
									if($produtos){
										foreach ($produtos as $produto) 
								{ ?>
									<tr>
										<td><?php echo $produto["id_produto"]; ?></td>
										<td><?php echo $produto["nome_produto"]; ?></td>
										<td><?php echo $produto["preco_produto"]; ?></td>
										<td><?php echo $produto["descricao_produto"]; ?></td>
										<td><?php echo $produto["qtd_produto"]; ?></td>
										<td><?php echo $produto["descricao_categoria"]; ?></td>
										<td>
										<a class="btn btn-default" href="<?php echo base_url('produto/editar_produto/'.$produto["id_produto"]); ?>">
											<i class="glyphicon glyphicon-pencil"></i>
										</a>
										<a class="btn btn-danger" href="<?php echo base_url('produto/deletar_produto/'.$produto["id_produto"]);?>"  onclick="return confirm('Deseja deletar este produto?')">
											<i class="glyphicon glyphicon-trash"></i>
										</a>
									</td>
									</tr>
								<?php 
									} //final do foreach
								  } else{
								?>
								<tr>
									<td colspan="3" class="text-center">Não há usuários cadastrados</td>
								</tr>
								<?php
									}
								?>
							</tbody>
					</table>
		  		</div>
		  	</div>
	  	</div>

	    