  	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Editar Produtos</h1>

	  	<div class="container">	
		  	<div class="row">
		  		<div class="col-md-10">
		  			<?php if ($alerta) {?>
				 		<div class="alert alert-<?php echo $alerta["class"]; ?>">

				 		<?php echo $alerta["mensagem"]; ?>

				 		</div>
			 		<?php } ?>

			 		<?php if ($produto) { ?>
			  			<form class="form-horizontal" action="<?php echo base_url('produto/editar_produto/'.$produto["id_produto"]); ?>" method="post">
			  			  <input type="hidden" name="captcha">
			  			  <input type="hidden" name="id_produto" value="<?php echo $produto["id_produto"]; ?>">
						  <div class="form-group">
						    <label for="nome" class="col-sm-2 control-label">Nome</label>
						    <div class="col-sm-10">
						      <input type="text" name="nome_produto"  class="form-control" id="nome" placeholder="Nome do produto" value="<?php echo set_value('nome_produto') ? set_value('nome_produto') : $produto["nome_produto"]; ?>" required>
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="preco" class="col-sm-2 control-label">Preço</label>
						    <div class="col-sm-10">
						      <input type="text" name="preco_produto" class="form-control" id="preco" placeholder="Preço do produto" value="<?php echo set_value('preco_produto') ? set_value('preco_produto') : $produto["preco_produto"]; ?>" required>
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="descricao" class="col-sm-2 control-label">Descrição</label>
						    <div class="col-sm-10">
						      <input type="text" name="descricao_produto" class="form-control" id="descricao" placeholder="Descrição do produto" value="<?php echo set_value('descricao_produto') ? set_value('descricao_produto') : $produto["descricao_produto"]; ?>" required>
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="quantidade" class="col-sm-2 control-label">Quantidade</label>
						    <div class="col-sm-10">
						      <input type="text" name="qtd_produto" class="form-control" id="quantidade" placeholder="Quantidade do produto" value="<?php echo set_value('qtd_produto') ? set_value('qtd_produto') : $produto["qtd_produto"]; ?>" required>
						    </div>
						  </div>
						  
						  <div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      <button type="submit" name="editar" value="editar" class="btn btn-success pull-right">Enviar</button>
						    </div>
						  </div>
						</form>
					<?php } ?>
		  		</div>
		  	</div>
	  	</div>

	    