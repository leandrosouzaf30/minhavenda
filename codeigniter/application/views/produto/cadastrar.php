  	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Cadastrar Produto</h1>

	  	<div class="container">	
		  	<div class="row">
		  		<div class="col-md-10">
		  			<?php if ($alerta) {?>
				 		<div class="alert alert-<?php echo $alerta["class"]; ?>">

				 		<?php echo $alerta["mensagem"]; ?>

				 		</div>
			 		<?php } ?>

			 			<form class="form-horizontal" action="<?php echo base_url('produto/cadastrar_produto'); ?>" method="post">
			  			  <input type="hidden" name="captcha">
			  			   <div class="form-group">
						    <label for="nome_cliente" class="col-sm-2 control-label">Nome</label>
						    <div class="col-sm-10">
						      <input type="text" name="nome_produto" class="form-control" id="nome" placeholder="Nome do produto">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="preco_produto" class="col-sm-2 control-label">Preço</label>
						    <div class="col-sm-10">
						      <input type="text" name="preco_produto" class="form-control" id="preco_produto" placeholder="Preço do produto">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="descricao_produto" class="col-sm-2 control-label">Descrição</label>
						    <div class="col-sm-10">
						      <input type="text" name="descricao_produto" class="form-control" id="descricao_produto" placeholder="Descrição do produto">
						    </div>
						  </div>
						  			
						  <div class="form-group">
						    	<label for="qtd_produto" class="col-sm-2 control-label">Quantidade</label>
						    <div class="col-sm-10">
						      <input type="text"  name="qtd_produto" class="form-control" id="qtd_produto" placeholder="Quantidade do produto">
						    </div>
						  </div>

						  <div class="form-group">
						    	<label for="categorias_id_categoria" class="col-sm-2 control-label">Categoria</label>
						    <div class="col-sm-10">
						      <select class="form-control" name="categorias_id_categoria">
						      	  <option value="0">Escolher Categoria</option>
								  <option value="1">Alimentos</option>
								  <option value="2">Casa e decoração</option>
								  <option value="3">Saúde e bem estar</option>
								  <option value="4">Tecnologia e eletrônicos</option>
								  <option value="5">Auto e ferramentas</option>
								  <option value="6">Outros</option>
							  </select>
						    </div>
						  </div>
						  
						  
						  <div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      <button type="submit" name="cadastrar" value="cadastrar" class="btn btn-success pull-right">Enviar</button>
						    </div>
						  </div>
						</form>
					
		  		</div>
		  	</div>
	  	</div>

	    