  	
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h4 class="page-header">Cadastrar novo Cliente  
          	<a class="btn btn-default" href="<?php echo base_url('cliente/cadastrar_cliente'); ?>">
				<i class="glyphicon glyphicon-plus"></i>
		  	</a>
		  </h4>

          <h1 class="">Clientes</h1>     

          <div class="container">
		  	<div class="row">
		  		<div class="col-md-12">
		  			<table class="table table-striped">
	  					<thead>
								<tr>
									<th>ID</th>
									<th>Nome</th>
									<th>Email</th>
									<th>Ações</th>
																	
								</tr>
							</thead>
							<tbody>
								<?php 
									if($clientes){
										foreach ($clientes as $cliente) 
								{ ?>
									<tr>
										<td><?php echo $cliente["id_cliente"]; ?></td>
										<td><?php echo $cliente["nome_cliente"]; ?></td>
										<td><?php echo $cliente["email_cliente"]; ?></td>
										<td>
										<a class="btn btn-default" href="<?php echo base_url('cliente/editar_cliente/'.$cliente["id_cliente"]); ?>">
											<i class="glyphicon glyphicon-pencil"></i>
										</a>
										<a class="btn btn-danger" href="<?php echo base_url('cliente/deletar_cliente/'.$cliente["id_cliente"]);?>""  onclick="return confirm('Deseja deletar este cliente?')">
											<i class="glyphicon glyphicon-trash"></i>
										</a>
									</td>
								</tr>
										
										
									</tr>
								<?php 
									} //final do foreach
								  } else{
								?>
								<tr>
									<td colspan="3" class="text-center">Não há usuários cadastrados</td>
								</tr>
								<?php
									}
								?>
							</tbody>
					</table>
		  		</div>
		  	</div>
  	</div>

   

         
        








  	