  	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Editar Clientes</h1>

	  	<div class="container">	
		  	<div class="row">
		  		<div class="col-md-10">
		  			<?php if ($alerta) {?>
				 		<div class="alert alert-<?php echo $alerta["class"]; ?>">

				 		<?php echo $alerta["mensagem"]; ?>

				 		</div>
			 		<?php } ?>

			 		<?php if ($cliente) { ?>
			  			<form class="form-horizontal" action="<?php echo base_url('cliente/editar_cliente/'. $cliente["id_cliente"]); ?>" method="post">
			  			  <input type="hidden" name="captcha">
			  			  <input type="hidden" name="id_cliente" value="<?php echo $cliente["id_cliente"]; ?>">
						  <div class="form-group">
						    <label for="nome_cliente" class="col-sm-2 control-label">Nome</label>
						    <div class="col-sm-10">
						      <input type="text" name="nome_cliente" class="form-control" id="nome" placeholder="Nome do Cliente" value="<?php echo set_value('nome_cliente') ? set_value('nome_cliente') : $cliente["nome_cliente"]; ?>">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="email_cliente" class="col-sm-2 control-label">Email</label>
						    <div class="col-sm-10">
						      <input type="email" name="email_cliente" class="form-control" id="email" placeholder="Email do Cliente" value="<?php echo set_value('email_cliente') ? set_value('email_cliente') : $cliente["email_cliente"]; ?>">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="senha" class="col-sm-2 control-label">Senha</label>
						    <div class="col-sm-10">
						      <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha">
						    </div>
						  </div>
						  			
						  <div class="form-group">
						    	<label for="confirmar_senha" class="col-sm-2 control-label">Confirmar senha</label>
						    <div class="col-sm-10">
						      <input type="password"  name="confirmar_senha" class="form-control" id="confirmar_senha" placeholder="Confirmar senha">
						    </div>
						  </div>
						  
						  
						  <div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      <button type="submit" name="editar" value="editar" class="btn btn-success pull-right">Enviar</button>
						    </div>
						  </div>
						</form>
					<?php } ?>
		  		</div>
		  	</div>
	  	</div>

	    