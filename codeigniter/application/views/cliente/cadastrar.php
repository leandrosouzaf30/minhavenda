  	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Cadastrar Cliente</h1>

	  	<div class="container">	
		  	<div class="row">
		  		<div class="col-md-10">
		  			<?php if ($alerta) {?>
				 		<div class="alert alert-<?php echo $alerta["class"]; ?>">

				 		<?php echo $alerta["mensagem"]; ?>

				 		</div>
			 		<?php } ?>

			 			<form class="form-horizontal" action="<?php echo base_url('cliente/cadastrar_cliente'); ?>" method="post">
			  			  <input type="hidden" name="captcha">
			  			   <div class="form-group">
						    <label for="nome_cliente" class="col-sm-2 control-label">Nome</label>
						    <div class="col-sm-10">
						      <input type="text" name="nome_cliente" class="form-control" id="nome" placeholder="Nome do Cliente">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="email_cliente" class="col-sm-2 control-label">Email</label>
						    <div class="col-sm-10">
						      <input type="email" name="email_cliente" class="form-control" id="email" placeholder="Email do Cliente">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="senha" class="col-sm-2 control-label">Senha</label>
						    <div class="col-sm-10">
						      <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha">
						    </div>
						  </div>
						  			
						  <div class="form-group">
						    	<label for="confirmar_senha" class="col-sm-2 control-label">Confirmar senha</label>
						    <div class="col-sm-10">
						      <input type="password"  name="confirmar_senha" class="form-control" id="confirmar_senha" placeholder="Confirmar senha">
						    </div>
						  </div>
						  
						  
						  <div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      <button type="submit" name="cadastrar" value="cadastrar" class="btn btn-success pull-right">Enviar</button>
						    </div>
						  </div>
						</form>
					
		  		</div>
		  	</div>
	  	</div>

	    