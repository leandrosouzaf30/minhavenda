<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model {

	
	public function get_clientes()

	{
		$query = $this->db->get('clientes');
		if ($query->num_rows()) {
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_cliente($id_cliente)
	{

		$this->db->where("id_cliente", $id_cliente);
		$cliente = $this->db->get('clientes');

		if($cliente->num_rows())
		{
			return $cliente->row_array();
		}
		else
		{
			return false;
		}
	}

	public function create_cliente($dados_cliente)
	{

		$this->db->insert('clientes', $dados_cliente);
		return $this->db->affected_rows() ? TRUE : FALSE;

	}

	public function update_cliente($id_cliente, $cliente_atualizado)
	{
		$this->db->where("id_cliente", $id_cliente);
		$this->db->update("clientes", $cliente_atualizado);

		if ($this->db->affected_rows()) 
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function delete_cliente($id_cliente)
	{
		$this->db->where('id_cliente', $id_cliente);
		$this->db->delete('clientes');

		if ($this->db->affected_rows()) {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}
