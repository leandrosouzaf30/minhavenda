<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto_model extends CI_Model {

	
	public function get_produtos()
	{

		/*$query = $this->db->get('produtos');
		if ($query->num_rows()) {
			return $query->result_array();
		}
		else
		{
			return false;
		}*/

		$this->db->select('*');
		$this->db->from('produtos');
		$this->db->join('categorias', 'produtos.categorias_id_categoria = categorias.id_categoria');

		$query = $this->db->get(); 

		if ($query->num_rows()) {
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_produto($id_produto)
	{
		$this->db->where("id_produto", $id_produto); //WHERE 'id_produto' = $id_produto
		$produto = $this->db->get('produtos'); //SELECT * FROM produtos WHERE 'id_produto' = $id_produto

		if ($produto->num_rows()) 
		{
			return $produto->row_array();
		}
		else
		{
			return false;
		}
	}

	public function create_produto($dados_produto)
	{
		
		$this->db->insert('produtos', $dados_produto);
		return $this->db->affected_rows() ? TRUE : FALSE;
	}

	public function update_produto($id_produto, $produto_atualizado)
	{
		$this->db->where("id_produto",$id_produto);
		$this->db->update("produtos", $produto_atualizado);

		if ($this->db->affected_rows()) 
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function delete_produto($id_produto)
	{
		$this->db->where('id_produto', $id_produto);
		$this->db->delete('produtos');

		if ($this->db->affected_rows()) {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
