<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido_model extends CI_Model {

	
	public function get_pedidos()
	{
		/*$query = $this->db->get('pedido_itens');
		if ($query->num_rows()) {
			return $query->result_array();
		}
		else
		{
			return false;
		}*/

		$this->db->select('*');
		$this->db->from('pedido_itens');
		$this->db->join('clientes', 'pedido_itens.clientes_id_cliente = clientes.id_cliente', 'inner');
		$this->db->join('produtos', 'pedido_itens.produtos_id_produto = produtos.id_produto', 'inner');


		$query = $this->db->get(); 

		if ($query->num_rows()) {
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
}
