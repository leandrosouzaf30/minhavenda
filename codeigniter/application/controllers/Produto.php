<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller {

	
	public function visualizar_todos()
	{
		$this->load->model('produto_model');
		$produtos = $this->produto_model->get_produtos();

		$dados = array(
			"produtos" => $produtos,
			"view" => 'produto/visualizar_todos'
		);

		
		$this->load->view('template', $dados);
	}

	public function cadastrar_produto()
	{
		$alerta = null;

		if ($this->input->post('cadastrar') && $this->input->post('cadastrar') === "cadastrar")
		{
			if ($this->input->post('captcha')) redirect('dashboard/index');

					//Definir regras de validação
					$this->form_validation->set_rules('nome_produto', 'NOME', 'required');
					$this->form_validation->set_rules('preco_produto', 'PRECO', 'required');
					$this->form_validation->set_rules('descricao_produto', 'DESCRICAO', 'required');
					$this->form_validation->set_rules('qtd_produto', 'QUANTIDADE', 'required');
					$this->form_validation->set_rules('categorias_id_categoria', 'CATEGORIA', 'required');

					if ($this->form_validation->run() === TRUE) 
					{
						$dados_produto = array(
							"nome_produto"      => $this->input->post('nome_produto'),
							"preco_produto"     => $this->input->post('preco_produto'),
							"descricao_produto" => $this->input->post('descricao_produto'),
							"qtd_produto"       => $this->input->post('qtd_produto'),
							"categorias_id_categoria"       => $this->input->post('categorias_id_categoria')
						);

						$this->load->model('produto_model');
						$cadastrado = $this->produto_model->create_produto($dados_produto);

						if ($cadastrado) 
						{
							
							//Produto cadastrado
							$alerta = array(
										"class" => "success",
										"mensagem" => "Produto cadastrado com sucesso!"
							);
						}
						else
						{
							//Erro cadastro
							$alerta = array(
										"class" => "danger",
										"mensagem" => "Atenção, Erro ao cadastrar produto!<br>".validation_errors()
							);
						}
					}
					else
					{
						//Dados Invalidos
						$alerta = array(
									"class" => "danger",
									"mensagem" => "Atenção, dados Invalidos".validation_errors()
						);
					}
		}
		

		$dados = array(
			"alerta" => $alerta,
			"view" => 'produto/cadastrar'
		);

		$this->load->view('template', $dados);
	}

	public function editar_produto($id_produto)
	{
		$alerta = null;
		$produto = null;

		$id_produto = (int) $id_produto;

		if ($id_produto) 
		{
				//Carrega o Model
			$this->load->model('produto_model');

			//Verifica se o produto está cadastrado no banco de dados
			$produto_existe = $this->produto_model->get_produto($id_produto);

			//Se o produto existir
			if ($produto_existe) {
				//Armazena o produto na variavel $produto
				$produto = $produto_existe;

				//Verifica se o post é verdadeiro 2
				if ($this->input->post('editar') === "editar") 
				{
					
					$id_produto_form = (int) $this->input->post('id_produto');


					//verificar se o captcha esta preenchido
					if ($this->input->post('captcha')) redirect('dashboard/index');
						//Verifica se os ids do form e da url coincidem
						if ($id_produto !== $id_produto_form) redirect('dashboard/index');

							//Definindo regras de validação
							$this->form_validation->set_rules('nome_produto', 'NOME', 'required');
							$this->form_validation->set_rules('preco_produto', 'PRECO', 'required');
							$this->form_validation->set_rules('descricao_produto', 'DESCRICAO', 'required');
							$this->form_validation->set_rules('qtd_produto', 'QUANTIDADE', 'required');

							if ($this->form_validation->run()) 
							{
								$produto_atualizado = array(
									"nome_produto" => $this->input->post('nome_produto'),
									"preco_produto" => $this->input->post('preco_produto'),
									"descricao_produto" => $this->input->post('descricao_produto'),
									"qtd_produto" => $this->input->post('qtd_produto')
								);


								$atualizado = $this->produto_model->update_produto($id_produto, $produto_atualizado);

								if ($atualizado) 
								{
									$alerta = array(
										"class" => "success",
										"mensagem" => "Produto atualizado com sucesso!"
									);	
								}
								else
						{
							//Erro na Atualização
							$alerta = array(
										"class" => "danger",
										"mensagem" => "Atenção, Não foi possivel atualizar os dados!"
							);
						}
					}
					else
					{
						//Formulario não validado
						$alerta = array(
									"class" => "danger",
									"mensagem" => "Atenção, Erro na validação dos dados do Formulario!<br>".validation_errors()
						);
					}
				}


			}
			else
			{
				$produto = false;
				//Produto não existe
				$alerta = array(
							"class" => "danger",
							"mensagem" => "Atenção, O Produto informado não existe!"
				);
			}
		}
		else
		{
			//Produto inválido
				$alerta = array(
							"class" => "danger",
							"mensagem" => "Atenção, O Produto informado é invalido!"
				);
		}
		$dados = array(

			"view" => 'produto/editar',
			"produto" => $produto,
			"alerta" => $alerta
		);

		$this->load->view('template', $dados);
	}

	public function deletar_produto($id_produto)
	{
		$alerta = null;
		
		//Faz o cast da variavel, ou seja, força a sua converção para inteiro
		$id_produto = (int) $id_produto;

		if ($id_produto) 
		{
			//Carrega o Model
			$this->load->model('produto_model');
			//Verifica se o produto está cadastrado no banco
			$produto_existe = $this->produto_model->get_produto($id_produto);
			if ($produto_existe) {
				$deletou = $this->produto_model->delete_produto($id_produto);

				if ($deletou) 
				{
					//Produto Deletado com sucesso
					$alerta = array(
								"class" => "success",
								"mensagem" => "Produto deletado com sucesso!"
					);
				}
				else
				{
					//Produto não deletado
					$alerta = array(
								"class" => "danger",
								"mensagem" => "Atenção, Erro ao deletar produto!"
					);
				}
			}
			else
			{
				//Produto não existe
				$alerta = array(
							"class" => "danger",
							"mensagem" => "Atenção, Produto não existe!"
				);
			}

		}
		else
		{
			//Produto invalido
			$alerta = array(
						"class" => "danger",
						"mensagem" => "Atenção, O produto informado está incorreto!"
			);
		}

		$dados = array(
			"alerta" => $alerta,
			
			"view" => 'produto/deletar'
		);

		
		$this->load->view('template', $dados);
	}
}
