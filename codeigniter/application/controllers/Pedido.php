<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido extends CI_Controller {

	
	public function visualizar_todos()
	{
		
		$this->load->model('pedido_model');
		$pedidos = $this->pedido_model->get_pedidos();

		$dados = array(
			"pedidos" => $pedidos,
			"view" => "pedido/visualizar_todos"
		);
		$this->load->view('template', $dados);
	}
}
