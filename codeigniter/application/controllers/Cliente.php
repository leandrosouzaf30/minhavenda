<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller 
{

	
	public function visualizar_todos()
	{
		$this->load->model('cliente_model');
		$clientes = $this->cliente_model->get_clientes();

		$dados = array(
			"clientes" => $clientes,
			"view" => 'cliente/visualizar_todos'
		);

		
		$this->load->view('template', $dados);
	}

	public function cadastrar_cliente()
	{
		$alerta = null;
		
		if ($this->input->post('cadastrar') && $this->input->post('cadastrar') === "cadastrar") 
		{
			if ($this->input->post('captcha')) redirect('dashboard/index');

					//Definir regras de validação
					$this->form_validation->set_rules('nome_cliente', 'NOME', 'required');
					$this->form_validation->set_rules('email_cliente', 'EMAIL', 'required|valid_email|is_unique[clientes.email_cliente]');
					$this->form_validation->set_rules('senha', 'SENHA', 'required|min_length[7]', array('required' => 'Você deve preencher a %s.'));
					$this->form_validation->set_rules('confirmar_senha', 'CONFIRMAR SENHA', 'required|min_length[7]|matches[senha]', array('required' => 'Você deve preencher a %s.'));

					if ($this->form_validation->run() === TRUE) 
					{

						$dados_cliente = array(
						"nome_cliente" =>$this->input->post('nome_cliente'),
						"email_cliente" => $this->input->post('email_cliente'),
						"senha" => $this->input->post('senha')
					);

						//Carrega o Model
						$this->load->model('cliente_model');
						$cadastrou = $this->cliente_model->create_cliente($dados_cliente);


						if ($cadastrou) 
						{
							//Cadastrado com sucesso
							$alerta = array(
										"class" => "success",
										"mensagem" => "Cadastro realizado com sucesso"
							);
						}
						else
						{
							//Erro no Cadastrado
							$alerta = array(
										"class" => "danger",
										"mensagem" => "Falha no cadastro".validation_errors()
							);
						}
					}
					else
					{
						//Falha na autenticação dos dados
							$alerta = array(
								"class" => "danger",
								"mensagem" => "Falha na autenticação dos dados!<br>".validation_errors()
							);
					}

				
		}
		

		$dados = array(
			"alerta" => $alerta,
			"view" => 'cliente/cadastrar'
		);
		$this->load->view('template', $dados);
	}

	public function editar_cliente($id_cliente)
	{

		$alerta = null;
		$cliente = null;

		//Faz o cast da variavel garantindo que ela seja um inteiro  1
		$id_cliente = (int) $id_cliente;

		if ($id_cliente) {

			//Carrega o Model 1
			$this->load->model('cliente_model');

			//Verifica se o cliente está cadastrado no banco de dados 1
			$cliente_existe = $this->cliente_model->get_cliente($id_cliente);
			
			
			if ($cliente_existe) {

				$cliente = $cliente_existe;
				//Verifica se o post é verdadeiro 2
				if ($this->input->post('editar') === "editar") 
				{

					$id_cliente_form = (int) $this->input->post('id_cliente');
					//verificar se o captcha esta preenchido
					if ($this->input->post('captcha')) redirect('dashboard/index');

					//Verifica se os ids do form e da url coincidem
					if ($id_cliente !== $id_cliente_form) redirect('dashboard/index');
					//Definindo regras de validação
					$this->form_validation->set_rules('nome_cliente', 'NOME', 'required');
					$this->form_validation->set_rules('email_cliente', 'EMAIL', 'required|valid_email');
					$this->form_validation->set_rules('senha', 'SENHA', 'required|min_length[7]', array('required' => 'Você deve preencher a %s.'));
					$this->form_validation->set_rules('confirmar_senha', 'CONFIRMAR SENHA', 'required|min_length[7]|matches[senha]', array('required' => 'Você deve preencher a %s.'));

					//Verificar se as regras foram atendidas
					if ($this->form_validation->run() === TRUE) 
					{
						//Array dos dados a serem atualizados
						$cliente_atualizado = array(
							"nome_cliente" => $this->input->post('nome_cliente'),
							"email_cliente" => $this->input->post('email_cliente'),
							"senha" => $this->input->post('senha')

						);

						$atualizado = $this->cliente_model->update_cliente($id_cliente, $cliente_atualizado);

						if ($atualizado)
						{
							//Cliente atualizado!!!
							$alerta = array(
										"class" => "success",
										"mensagem" => "Cliente atualizado com sucesso!"
							);	
						}
						else
						{
							//Erro na Atualização
							$alerta = array(
										"class" => "danger",
										"mensagem" => "Atenção, Não foi possivel atualizar os dados!"
							);
						}
					}
					else
					{
						//Formulario não validado
						$alerta = array(
									"class" => "danger",
									"mensagem" => "Atenção, Erro na validação dos dados do Formulario!<br>".validation_errors()
						);
					}
				}


			}
			else
			{
				$cliente = false;
				//Cliente não existe
				$alerta = array(
							"class" => "danger",
							"mensagem" => "Atenção, O Cliente informado não existe!"
				);
			}
		}
		else
		{
			//Cliente inválido
				$alerta = array(
							"class" => "danger",
							"mensagem" => "Atenção, O Cliente informado é invalido!"
				);
		}
		$dados = array(

			"view" => 'cliente/editar',
			"cliente" => $cliente,
			"alerta" => $alerta
		);

		$this->load->view('template', $dados);
	}

	public function deletar_cliente($id_cliente)
	{
		$alerta = null;

		$id_cliente = (int) $id_cliente;

		if ($id_cliente) {
			$this->load->model('cliente_model');


			$cliente_existe = $this->cliente_model->get_cliente($id_cliente);
			if ($cliente_existe) 
			{
				$deletou = $this->cliente_model->delete_cliente($id_cliente);

				if ($deletou) {
					//Cliente deletado
					$alerta = array(
								"class" => "success",
								"mensagem" => "Cliente deletado com sucesso!"
					);
				}
				else
				{
					//Cliente nao deletado
					$alerta = array(
								"class" => "danger",
								"mensagem" => "Atenção, Cliente não deletado!"
					);
				}
			}
			else
			{
				//Cliente inexistente
				$alerta = array(
							"class" => "danger",
							"mensagem" => "Atenção, O cliente informado não existe!"
				);
			}

		}
		else
		{
			//Cliente invalido
			$alerta = array(
						"class" => "danger",
						"mensagem" => "Atenção, O cliente informado está incorreto!"
			);
		}
		$dados = array(
			"alerta" => $alerta,
			
			"view" => 'cliente/deletar'
		);

		
		$this->load->view('template', $dados);
	}
}
